"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.contrib.auth import views as auth_views

import dailymoodlog.views

from tastypie.api import Api
from dailymoodlog.api import EmotionResource

v1_api = Api(api_name='v1')
v1_api.register(EmotionResource())


urlpatterns = patterns('',
    # Examples:
    url(r'^$', dailymoodlog.views.HomeView.as_view(), name='base'),
    url(r'^dailymoodlog/login/$', dailymoodlog.views.HomeView.as_view(), name='login'),

    url(r'^dailymoodlog/emotion-before/', dailymoodlog.views.get_emotions, name='emotion-before'),
    #url(r'^dailymoodlog/emotion-before/', dailymoodlog.views.EmotionView, name ='emotion-before'),
    url(r'^dailymoodlog/emotion-before-saved/', dailymoodlog.views.filtered_emotions, name = 'emotion-before-saved'),

    url(r'^api/', include(v1_api.urls)),

    url(r'^dailymoodlog/edit_thought/', dailymoodlog.views.CreateThoughtView.as_view(), name='edit_thought'),

    url(r'^dailymoodlog/emotion-after/', dailymoodlog.views.emotions_ratings_after, name = 'emotion-after'),

    url(r'^dailymoodlog/thought-list/', dailymoodlog.views.PositiveThoughtView.as_view(), name='thought-list'),

    url(r'^dailymoodlog/progress/', dailymoodlog.views.FinishedView.as_view(), name='progress'),

    #url(r'^lists/', include('lists.urls')),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)

#urlpatterns += staticfiles_urlpatterns()