__author__ = 'szeitlin'

from tastypie.resources import ModelResource
from dailymoodlog.models import Emotion

class EmotionResource(ModelResource):
    class Meta:
        queryset = Emotion.objects.all()
        resource_name = 'emotion'

