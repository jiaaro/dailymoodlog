__author__ = 'szeitlin'

#template tags required for SelectMultipleField lists of Emotion and Distortion choices

emotion_choice_list = ('Sad', 'depressed', 'unhappy',
     'Anxious', 'worried', 'panicky', 'nervous', 'scared',
     'Guilty', 'ashamed',
     'Inferior', 'worthless', 'inadequate', 'defective', 'incompetent',
     'Lonely', 'unloved', 'unwanted', 'rejected', 'alone', 'abandoned', 'isolated',
     'Embarassed', 'foolish', 'humiliated', 'self-conscious', 'awkward', 'clumsy', 'ham-handed',
     'Hopeless', 'discouraged', 'pessimistic', 'despairing',
     'Frustrated', 'stuck', 'defeated', 'confused', 'unmoored', 'lost',
     'Angry','mad', 'resentful', 'annoyed', 'irritated', 'upset', 'furious', 'aggravated')


distortion_choice_list = (
    "all-or-nothing",
    "overgeneralization",
    "mental filter: dwelling on the negative",
    "discounting the positives",
    "jumping to conclusions",
    "mind-reading",
    "fortune-telling",
    "magnification",
    "emotional reasoning",
    "should statements",
    "labeling yourself",
    "taking things too personally",
    "blaming yourself",
)

def decode_choice(choice_list):
    """
    this is only necessary if you use a dictionary, and I'm not sure why you would do that?
    :param choice_list:
    :return:
    """
    pass
