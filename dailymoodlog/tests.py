from django.test import TestCase
from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import HomeView, EmotionView, CreateThoughtView, DistortionView

from dailymoodlog.models import DailyLog, Thought, Emotion, Distortion

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        self.assertTrue(response.content.startswith(b'<html>'))
        self.assertIn(b'<title>Daily Mood Log</title>', response.content)
        self.assertTrue(response.content.endswith(b'</html>'))

class DailyLogTests(TestCase):

    def test_can_create_and_return_a_log(self):
        d = DailyLog()
        self.assertEqual(d.created, timezone.now().date())

class ThoughtTests(TestCase):

    def test_can_create_and_return_a_thought(self):
        t = Thought(thought_text="I'm afraid of job interviews")
        self.assertEqual(t.thought_text, "I'm afraid of job interviews")

class EmotionTests(TestCase):

    def test_can_choose_and_return_an_emotion(self):
        e = Emotion.objects.get(emotion_text='Sad')
        self.assertEqual(e.emotion_text, 'Sad')


class DistortionTests(TestCase):

    def test_can_choose_and_return_a_distortion(self):
        d = Distortion.objects.get(distortion_text='self-immolation')
        self.assertEqual(d.distortion_text, 'self-immolation')


