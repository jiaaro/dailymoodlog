__author__ = 'szeitlin'

#so this looks great, have to add AHAH to save the data


import django_tables2 as tables
from django_tables2 import SingleTableView
from dailymoodlog.models import Emotion



class EmotionTable(tables.Table):

    id = tables.Column(visible=False)
    checked_emotion = tables.CheckBoxColumn(accessor='pk')
    emotion_rating_before = tables.EditableColumn()

    class Meta:
        model=Emotion
        exclude = ['emotionlist', 'dailylog', 'emotion_rating_after']


class EmotionAfterTable(tables.Table):

    emotion_rating_after = tables.EditableColumn()
    emotion_text = tables.Column(accessor='ok')
    selected = tables.CheckBoxColumn(accessor="selector")

    class Meta:
        model=Emotion
        exclude = ['id', 'emotionlist', 'dailylog']

    # def render_selected(self,record):
    #     if record.selected:
    #         return mark_safe('<input class="emotionCheckBox" name="emotion_text[]" type="checkbox" checked/>
    #     else:
    #         return mark_safe('<input class="emotionCheckBox" name="emotion_text[]" type="checkbox"/>


class EditCategoryTable(tables.Table):
    name = tables.Column(accessor='name')
    selected = tables.CheckBoxColumn(accessor="selected")


#from http://kuttler.eu/post/using-django-tables2-filters-crispy-forms-together/

class FilteredEmotionTable(SingleTableView):
    filter_class = None
    formhelper_class = None
    context_filter_name = 'filter'

    def get_queryset(self, **kwargs):
        qs = super(FilteredEmotionTable, self).get_queryset()
        self.filter = self.filter_class(self.request.GET, queryset = qs)
        self.filter.form.helper = self.formhelper_class()
        return self.filter.qs

    def get_table(self, **kwargs):
        table = super(FilteredEmotionTable, self).get_table()
        return table

    def get_context_data(self, **kwargs):
        context  = super(FilteredEmotionTable, self).get_context_data()
        context[self.context_filter_name] = self.filter
        return context

