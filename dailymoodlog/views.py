from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from dailymoodlog.models import DailyLog, Thought, Emotion, Distortion, ThoughtList, EmotionsChecklist, DistortionsChecklist

from django.views.generic.edit import FormView
from django.views.generic import CreateView, ListView, DetailView
from django.utils import timezone

from dailymoodlog.forms import UserForm, EmotionAfterForm, ThoughtBeforeForm, ThoughtAfterForm #EmotionForm,
from django.forms.models import modelformset_factory, inlineformset_factory

from django import forms
from django.forms import formset_factory

from django_tables2 import RequestConfig
from dailymoodlog.tables import EmotionTable, EmotionAfterTable

from django.contrib.auth.decorators import login_required

# import datatableview
# from datatableview.views import DatatableView


from django_datatables_view.base_datatable_view import BaseDatatableView

from django.utils import timezone



#use this by putting @login_required before view methods

class HomeView(FormView):

    model = DailyLog
    template_name = 'dailymoodlog/base.html'
    fields = ['created']
    form_class = UserForm

    def home_page(request):
        return render(request, 'base.html')

    def create_user(request):

        userform = UserForm(request.POST or None)

        if request.POST and userform.is_valid():
            userform.save()
            return HttpResponseRedirect(reverse('base.html'))

        return render_to_response('base.html', {'form': userform})

# class EmotionView(CreateView):
#
#     model = Emotion
#     template_name = 'dailymoodlog/emotion-before.html'
#     fields = ['emotion_text', 'checked_emotion', 'emotion_rating_before', 'emotion_rating_after']
#     success_url = 'dailymoodlog/edit-thought.html'

def get_emotions(request):
    """ Using django_tables2, & will require AHAH to save the data.
    See tables.py file for configuration info."""
#    if request.GET:
    table = EmotionTable(Emotion.objects.all())
    RequestConfig(request, paginate=False).configure(table)
    return render(request, "emotion-before-saved.html", {"table": table})

def filtered_emotions(response):
    """idea here is that jQuery AJAX script will save the data to the database,
    and this will render the filtered list before the user goes on to the next step"""
    today = timezone.now().date()
    table = EmotionTable(Emotion.objects.filter(dailylog__pub_date__lte=today, checked_emotion=True))
    RequestConfig(response, paginate=False).configure(table)
    return render(response, 'emotion-before-saved.html', {"table": table})

def emotions_ratings_after(request):
    today = timezone.now().date()
    table = EmotionAfterTable(Emotion.objects.filter(dailylog__pub_date__lte=today, checked_emotion=True))
    RequestConfig(request, paginate=False).configure(table)
    return render(request, 'emotion-after.html', {"table": table})



class EmotionNotEditableDatatableView(BaseDatatableView):
    model = Emotion
    template_name='dailymoodlog/emotion.html'

    order_columns = ['emotion_text', 'checked_emotion', 'emotion_rating_before']

    def get_initial_queryset(self):
        return Emotion.objects.all()

    def prepare_results(self, qs):
        json_data = []
        for item in qs:
            json_data.append([
                "{0}".format(item.emotion_text)
            ])
        return json_data

    def render_column(self, row, column, json_data):
        if column == 'emotion_text':
            return json_data
        else:
            return super(EmotionNotEditableDatatableView, self).render_column(row,column)



class EmotionAfterView(FormView):

    model = Emotion
    template_name='dailymoodlog/emotion-after.html'
    form_class = EmotionAfterForm
    success_url = 'dailymoodlog/thought-list/' #success url is required

    def get_emotions(request):

        emotionform = EmotionAfterForm(request.POST or None)

        if request.POST and emotionform.is_valid():
            emotionform.save()
            return HttpResponseRedirect(reverse('emotion-after'))

        return render(request, 'emotion-after.html', {'form': emotionform})

class EmotionRatingAfterView(FormView):

    model = Emotion
    template_name = 'dailymoodlog/emotion-after.html'
    form_class = EmotionAfterForm

    #dynamically assign slots in the form
    tomorrow = timezone.now().date()
    querycount = Emotion.objects.filter(dailylog__created__lte=tomorrow).count()

    #would like to set this up as default to today, otherwise let them enter a different day to search for?

    form_class = formset_factory(form=EmotionAfterForm, extra = querycount)

    def rate_after(request, emotion_id):
        """
        Only want to display and rate the ones that were selected and rated before
        :param request:
        :param emotion_id:
        :return:
        """
        p = get_object_or_404(Emotion, pk = emotion_id)
        try:
            emotion_rating_after = p.get(pd=request.POST['emotion_rating_after'])

        except KeyError:

            return render(request, 'dailymoodlog/emotion-after.html', {
                'emotion':p,
                'error_message': "please rate these emotions again after identifying cognitive distortions",
            })
        return HttpResponseRedirect(reverse('dailymoodlog:emotion-after', args=(p.id,)))

class CreateThoughtView(FormView):
    """maybe base this one on what we did for the todo lists? but convert to class-based views?"""

    model = Thought
    template_name = 'dailymoodlog/edit_thought.html'
    form_class = ThoughtBeforeForm
    form_class = formset_factory(form=ThoughtBeforeForm, extra = 4)

    success_url = '/dailymoodlog/emotion-after/'

    def get_thoughts(request): #, thoughtformset):

        thoughtform = ThoughtBeforeForm(request.POST or None)

        if request.POST and thoughtform.is_valid():
            thoughtform.save()
            return HttpResponseRedirect(reverse('edit_thought'))

        return render_to_response(request, 'edit_thought.html', {'form': thoughtform})


class PositiveThoughtView(FormView):
    """want to filter by publication_date"""

    model = Thought
    template_name = 'dailymoodlog/thought-list.html'
    form_class = ThoughtAfterForm
    form_class = formset_factory(form=ThoughtAfterForm, extra = 4)
    success_url = '/dailymoodlog/progress/'
    #context_object_name = 'latest_thought-list'


    def get_existing_thoughts(request, latest_thought_list):
        """

        Note that request.POST != "if request.method == 'POST' "
        request.POST can handle empty forms, the other is required to pass data.

        """

        thoughtform = ThoughtAfterForm(request.POST or None)

        if request.method == "POST" and thoughtform.is_valid():
            thoughtform.save()
            return HttpResponseRedirect(reverse('thought-list'))
        # else:
        #     thought_text = forms.CharField(queryset = latest_thought_list)

        return render_to_response(request, 'thought-list.html', {'form': thoughtform})


class FinishedView(FormView):

    model=DailyLog
    template_name = 'dailymoodlog/progress.html'
    # form_class = UserForm
    # fields = ['username', 'email', 'password']
    success_url = 'dailymoodlog/base.html'

    def get_success_page(request):
        """
        This one has to retrieve existing thoughts and before-ratings.

        Note that request.POST != "if request.method == 'POST' "
        request.POST can handle empty forms, the other is required to pass data.

        """

        finishedform = UserForm(request.POST or None)


        if request.method == "POST" and finishedform.is_valid():
            finishedform.save()
            return HttpResponseRedirect(reverse('progress'))

        return render(request, 'progress.html', {'form': finishedform})

