# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0011_auto_20150709_1250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='distortion',
            name='distortionlist',
            field=models.ForeignKey(to='dailymoodlog.DistortionsChecklist', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotionlist',
            field=models.ForeignKey(to='dailymoodlog.EmotionsChecklist', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='thought',
            name='thoughtlist',
            field=models.ForeignKey(to='dailymoodlog.ThoughtList', blank=True, null=True),
        ),
    ]
