# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0018_auto_20150716_0957'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyLogList',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('pub_date', models.DateField(null=True, blank=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='dailylog',
            name='published_date',
        ),
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateField(default=datetime.date(2015, 7, 21)),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotion_text',
            field=models.CharField(max_length=250, default=''),
        ),
        migrations.AddField(
            model_name='emotion',
            name='pub_date',
            field=models.ForeignKey(to='dailymoodlog.DailyLogList', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='thought',
            name='pub_date',
            field=models.ForeignKey(to='dailymoodlog.DailyLogList', null=True, blank=True),
        ),
    ]
