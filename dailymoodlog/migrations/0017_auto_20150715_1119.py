# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0016_auto_20150715_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name='distortion',
            name='distortionlist',
            field=models.ForeignKey(null=True, blank=True, to='dailymoodlog.DistortionsChecklist'),
        ),
        migrations.AddField(
            model_name='emotion',
            name='checked_emotion',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='distortion',
            name='checked_distortion',
            field=models.NullBooleanField(default=False),
        ),
    ]
