# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0008_auto_20150709_0942'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emotion',
            name='emotionratingafter',
        ),
        migrations.RemoveField(
            model_name='emotion',
            name='emotionratingbefore',
        ),
        migrations.RemoveField(
            model_name='thought',
            name='thoughtratingafter',
        ),
        migrations.RemoveField(
            model_name='thought',
            name='thoughtratingbefore',
        ),
        migrations.AddField(
            model_name='dailylog',
            name='created',
            field=models.DateField(default=datetime.date(2015, 7, 9)),
        ),
        migrations.AddField(
            model_name='distortion',
            name='distortion_text',
            field=models.TextField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='emotion',
            name='emotion_text',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='emotion',
            name='emotionrating_after',
            field=models.IntegerField(default=50),
        ),
        migrations.AddField(
            model_name='emotion',
            name='emotionrating_before',
            field=models.IntegerField(default=50),
        ),
        migrations.AddField(
            model_name='thought',
            name='thoughtrating_after',
            field=models.IntegerField(default=50),
        ),
        migrations.AddField(
            model_name='thought',
            name='thoughtrating_before',
            field=models.IntegerField(default=50),
        ),
        migrations.AlterField(
            model_name='distortion',
            name='distortionlist',
            field=models.ForeignKey(to='dailymoodlog.DistortionsChecklist', default=None),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotionlist',
            field=models.ForeignKey(to='dailymoodlog.EmotionsChecklist', default=None),
        ),
        migrations.AlterField(
            model_name='thought',
            name='thought_text',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='thought',
            name='thoughtlist',
            field=models.ForeignKey(to='dailymoodlog.ThoughtList', default=None),
        ),
    ]
