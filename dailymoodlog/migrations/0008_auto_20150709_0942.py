# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0007_auto_20150708_0824'),
    ]

    operations = [
        migrations.CreateModel(
            name='Distortion',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('checked_distortion', models.NullBooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='DistortionsChecklist',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
            ],
        ),
        migrations.CreateModel(
            name='Emotion',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('checked_emotion', models.NullBooleanField()),
                ('emotionratingbefore', models.IntegerField()),
                ('emotionratingafter', models.IntegerField()),
                ('emotionlist', models.ForeignKey(to='dailymoodlog.EmotionsChecklist')),
            ],
        ),
        migrations.RemoveField(
            model_name='emotions',
            name='emotionlist',
        ),
        migrations.DeleteModel(
            name='PercentRating',
        ),
        migrations.AddField(
            model_name='thought',
            name='thoughtratingafter',
            field=models.IntegerField(default=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='thought',
            name='thoughtratingbefore',
            field=models.IntegerField(default=50),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='Emotions',
        ),
        migrations.AddField(
            model_name='distortion',
            name='distortionlist',
            field=models.ForeignKey(to='dailymoodlog.DistortionsChecklist'),
        ),
    ]
