# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0002_dailylog'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateTimeField(verbose_name=datetime.datetime(2015, 7, 1, 15, 33, 12, 696370, tzinfo=utc)),
        ),
    ]
