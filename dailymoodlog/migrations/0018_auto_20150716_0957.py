# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0017_auto_20150715_1119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateField(default=datetime.date(2015, 7, 16)),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotion_text',
            field=models.CharField(max_length=250, choices=[('Sad', (('Sad', 'Sad'), ('depressed', 'depressed'), ('unhappy', 'unhappy'))), ('Anxious', (('Anxious', 'Anxious'), ('worried', 'worried'), ('panicky', 'panicky'), ('nervous', 'nervous'), ('scared', 'scared'))), ('Guilty', (('Guilty', 'Guilty'), ('ashamed', 'ashamed'))), ('Inferior', (('Inferior', 'Inferior'), ('worthless', 'worthless'), ('inadequate', 'inadequate'), ('defective', 'defective'), ('incompetent', 'incompetent'))), ('Lonely', (('Lonely', 'Lonely'), ('unloved', 'unloved'), ('unwanted', 'unwanted'), ('rejected', 'rejected'), ('alone', 'alone'), ('abandoned', 'abandoned'), ('isolated', 'isolated'))), ('Embarassed', (('Embarassed', 'Embarassed'), ('foolish', 'foolish'), ('humiliated', 'humiliated'), ('self-conscious', 'self-conscious'), ('awkward', 'awkward'), ('clumsy', 'clumsy'), ('ham-handed', 'ham-handed'))), ('Hopeless', (('Hopeless', 'Hopeless'), ('discouraged', 'discouraged'), ('pessimistic', 'pessimistic'), ('despairing', 'despairing'))), ('Frustrated', (('Frustrated', 'Frustrated'), ('stuck', 'stuck'), ('defeated', 'defeated'), ('confused', 'confused'), ('unmoored', 'unmoored'), ('lost', 'lost'))), ('Angry', (('Angry', 'Angry'), ('mad', 'mad'), ('resentful', 'resentful'), ('annoyed', 'annoyed'), ('irritated', 'irritated'), ('upset', 'upset'), ('furious', 'furious'), ('aggravated', 'aggravated')))], default=''),
        ),
        migrations.AlterField(
            model_name='thought',
            name='thought_text',
            field=models.CharField(max_length=250, default=''),
        ),
    ]
