__author__ = 'szeitlin'

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_find_the_host(self):
        self.browser.get('http://localhost:8000')

        self.assertIn('Daily Mood Log', self.browser.title)

    def test_date_is_displayed(self):
        pass

    def test_start_button_links_to_emotions_before_page(self):
        pass

    def test_can_select_and_rate_an_emotion(self):
        self.browser.find_element_by_id('id_emotion_choices_0').click()

        inputbox = self.browser.find_element_by_id('id_emotion_rating_before')
        inputbox.send_keys('100')

        #do a few more

        #submit selections
        self.browser.find_element_by_name('Submit').click()

        #confirm that it sends you back to the main page or on to the next step?

    def test_can_enter_a_thought_and_rate_it(self):
        inputbox = self.browser.find_element_by_id('id_thought_text')
        inputbox.send_keys("I'm afraid this won't work at all")

        inputbox = self.browser.find_element_by_id('id_thought_rating_before')
        inputbox.send_keys('75')

        #add a couple more here

        #submit selections
        savebox = self.browser.find_element_by_name('save_thoughts').click()

    def test_can_select_and_save_distortion_choices(self):
        """
        would be cute to hook this up to randomly generate the int part of the distortion id to test.

        """
        self.browser.find_element_by_id('id_distortion_choices_0').click()

        #maybe add a couple more?

        #submit selections
        self.browser.find_element_by_name('selected_distortions').click()


if __name__=='__main__':
    unittest.main(warnings='ignore') #known bug in selenium

